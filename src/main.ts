import { createApp } from 'vue'
import App from './App.vue'
import { createI18n, SUPPORT_LOCALES } from './i18n'
import { router } from './router'
import { data } from './data'


const i18n = createI18n()

router.beforeEach((to, from, next) => {

    if (to.meta.useLanguage) {
        let language: string;
        if (to.params.lang)
            language = (to.params.lang as string).substring(1);
        else { language = 'en' }

        if (language == 'en')
            to.params.lang = null;


        if (SUPPORT_LOCALES.find(x => x == language.toLowerCase())) {
            i18n.global.locale.value = language;

            let item = data[language].find(x => x == to.name);
            if (!item) {
                next({ name: data[language][0], params: { lang: to.params.lang } })
            }
        }
    }

    next()
})



createApp(App)
    .use(i18n)
    .use(router)
    .mount('#app')
