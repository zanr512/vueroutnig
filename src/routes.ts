import { RouteRecordRaw } from 'vue-router';

import layout from './pages/layout.vue'
import navodilo1 from './pages/navodilo1.vue'
import navodilo2 from './pages/navodilo2.vue'
import navodilo3 from './pages/navodilo3.vue'
import landingPage from './pages/landingPage.vue'

const routes: RouteRecordRaw[] = [
    {
        path: '/',
        redirect: 'user-manual'
    },
    {
        path: '/user-manual',
        component: layout,
        children: [
            {
                path: '',
                component: landingPage,
                name: 'userManual'
            },
            {
                path: 'navodilo-1:lang(\-en|\-ja|\-sl)?',
                component: navodilo1,
                name: 'navodilo1',
                meta: {
                    useLanguage: true
                }
            },
            {
                path: 'navodilo-2:lang(\-en|\-ja|\-sl)?',
                name: 'navodilo2',
                component: navodilo2,
                meta: {
                    useLanguage: true
                }
            },
            {
                path: 'navodilo-3:lang(\-sl)?',
                name: 'navodilo3',
                component: navodilo3,
                meta: {
                    useLanguage: true
                }
            }
        ]
    },

    // Always leave this as last one,
    // but you can also remove it
    {
        path: '/:catchAll(.*)*',
        component: () => import('./pages/404.vue'),
    },
];

export default routes;
