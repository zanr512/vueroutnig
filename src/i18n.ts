import { createI18n as _createI18n } from 'vue-i18n'
import en from './locales/en.json'
import ja from './locales/ja.json'
import sl from './locales/sl.json'

// const messageImports = import.meta.glob('./locales/*.json')
// console.log(messageImports)

export const SUPPORT_LOCALES = ['en', 'ja', 'sl']

export function createI18n() {
    return _createI18n({
        legacy: false,
        globalInjection: true,
        locale: 'sl',
        fallbackLocale: 'en',
        messages: {
            en,
            ja,
            sl
        }
    })
}