import {
    createRouter,
    createWebHistory
} from 'vue-router'

import routes from './routes'

// const router = createRouter({
//     // use appropriate history implementation for server/client
//     // import.meta.env.SSR is injected by Vite.
//     history: createWebHistory(),
//     routes
// })

// router.beforeEach((to, from, next) => {
//     let lang = to.params.lang;
// })

export const router = createRouter({
    // use appropriate history implementation for server/client
    // import.meta.env.SSR is injected by Vite.
    history: createWebHistory(),
    routes
})